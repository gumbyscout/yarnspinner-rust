mod utils;

#[macro_use]
extern crate serde_derive;

use wasm_bindgen::prelude::*;
use yarn_spinner::{parse, CompiledLine, Compiler};

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub struct YarnSpinner {
    compiler: Box<JsValue>,
}

#[wasm_bindgen]
impl YarnSpinner {
    #[wasm_bindgen(constructor)]
    pub fn new(starting_node: &str) -> Self {
        Self {
            compiler: Box::new(JsValue::from_serde(&Compiler::new(starting_node)).unwrap()),
        }
    }

    pub fn compile(&self, file: &str) -> Result<(), JsValue> {
        let mut compiler: Compiler = self.compiler.into_serde().map_err(|e| format!("{:?}", e))?;
        let parsed = parse(file);
        compiler.compile(parsed).map_err(|e| JsValue::from(e))
    }

    pub fn next(&self) -> Result<JsValue, JsValue> {
        let mut compiler: Compiler = self.compiler.into_serde().map_err(|e| format!("{:?}", e))?;
        JsValue::from_serde(&compiler.next()).map_err(|e| JsValue::from(format!("{:?}", e)))
    }

    pub fn answer(&self, option: &str) -> Result<(), JsValue> {
        let mut compiler: Compiler = self.compiler.into_serde().map_err(|e| format!("{:?}", e))?;
        compiler.answer(option).map_err(|e| JsValue::from(e))
    }
}
