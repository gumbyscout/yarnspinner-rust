use nom::{
    branch::alt,
    bytes::complete::{is_not, tag, take_while1},
    character::{
        complete::{alphanumeric1, digit1, line_ending, space0, space1},
        is_alphanumeric,
    },
    combinator::{all_consuming, map, map_res, opt, peek, verify},
    error::ErrorKind,
    multi::{many1, separated_list, separated_nonempty_list},
    sequence::{delimited, pair, preceded, separated_pair, terminated},
    Err::Error,
    IResult,
};

use super::Num;

#[derive(Debug, PartialEq, Clone)]
pub enum Value<'a> {
    Word(&'a str),
    Number(Num),
    List(Vec<&'a str>),
    Coordinate((Num, Num)),
}

pub type Property<'a> = (&'a str, Option<Value<'a>>);

#[derive(Debug, PartialEq, Clone)]
pub enum Command {
    Conditional,
}

type Shortcut<'a> = (&'a str, Vec<Line<'a>>, Option<&'a str>, Option<Command>);

#[derive(Debug, PartialEq, Clone)]
pub enum Line<'a> {
    Dialogue((Option<&'a str>, &'a str, Option<&'a str>)),
    Shortcuts(Vec<Shortcut<'a>>),
    Options(Vec<(&'a str, &'a str, Option<&'a str>)>),
    Jump(&'a str),
    Command(Command),
}

#[derive(Debug, PartialEq, Clone)]
pub struct Node<'a> {
    pub meta: Vec<Property<'a>>,
    pub lines: Vec<Line<'a>>,
}

fn end_of_line(i: &str) -> IResult<&str, &str> {
    preceded(space0, line_ending)(i)
}

fn header_end(i: &str) -> IResult<&str, &str> {
    terminated(tag("---"), end_of_line)(i)
}

fn word(i: &str) -> IResult<&str, &str> {
    take_while1(|c: char| is_alphanumeric(c as u8) || c == '-' || c == '_')(i)
}

fn number(i: &str) -> IResult<&str, Num> {
    map_res(digit1, |d: &str| d.parse::<Num>())(i)
}

fn string(i: &str) -> IResult<&str, &str> {
    map(
        verify(is_not("#|\n"), |s: &str| !s.contains("===")),
        |s: &str| s.trim(),
    )(i)
}

fn l10n_tag(i: &str) -> IResult<&str, &str> {
    preceded(tag("#line:"), alphanumeric1)(i)
}

fn l10n_string(i: &str) -> IResult<&str, (&str, Option<&str>)> {
    pair(string, opt(l10n_tag))(i)
}

fn colon_separator(i: &str) -> IResult<&str, &str> {
    delimited(space0, tag(":"), space0)(i)
}

fn property(i: &str) -> IResult<&str, (&str, Option<&str>)> {
    terminated(
        separated_pair(word, colon_separator, opt(string)),
        end_of_line,
    )(i)
}

fn property_with_value(i: &str) -> IResult<&str, (&str, &str)> {
    let (i, (key, val)) = verify(property, |(_, v)| v.is_some())(i)?;
    Ok((i, (key, val.unwrap())))
}

// TODO: Can we clean up property values using peek or something?
// These all feel gross and hacky

fn list(i: &str) -> IResult<&str, Property> {
    let (k, (key, val)) = property_with_value(i)?;
    let (_, l) = separated_list(space1, word)(val)?;
    if l.len() < 2 {
        return Err(Error((i, ErrorKind::Verify)));
    }
    Ok((k, (key, Some(Value::List(l)))))
}

fn coordinate(i: &str) -> IResult<&str, Property> {
    let (k, (key, val)) = property_with_value(i)?;
    let (_, p) = separated_pair(number, tag(","), number)(val)?;
    Ok((k, (key, Some(Value::Coordinate(p)))))
}

fn prop_word(i: &str) -> IResult<&str, Property> {
    let (k, (key, val)) = property_with_value(i)?;
    let res = all_consuming(word)(val);
    if res.is_err() {
        return Err(Error((i, ErrorKind::Verify)));
    }
    let (_, w) = res.unwrap();
    Ok((k, (key, Some(Value::Word(w)))))
}

fn properties(i: &str) -> IResult<&str, Property> {
    alt((
        coordinate,
        prop_word,
        list,
        map(property, |(k, _)| (k, None)),
    ))(i)
}

fn header(i: &str) -> IResult<&str, Vec<Property>> {
    terminated(many1(properties), header_end)(i)
}

fn dialogue(i: &str) -> IResult<&str, Line> {
    map(
        pair(opt(terminated(word, colon_separator)), l10n_string),
        |(s, (d, l))| Line::Dialogue((s, d, l)),
    )(i)
}

fn shortcut(i: &str) -> IResult<&str, Shortcut> {
    // The shortcut itself
    // TODO: Parse command if it exists
    let (i, (t, l)) = preceded(pair(tag("->"), space0), l10n_string)(i)?;
    // Its nested children
    let peek_children = peek(pair(end_of_line, space1))(i);
    if peek_children.is_err() {
        return Ok((i, (t, vec![], l, None)));
    }
    let (i, ls) = preceded(
        end_of_line,
        separated_list(end_of_line, preceded(space1, dialogue)),
    )(i)?;
    Ok((i, (t, ls, l, None)))
}

fn shortcuts(i: &str) -> IResult<&str, Line> {
    map(separated_nonempty_list(end_of_line, shortcut), |s| {
        Line::Shortcuts(s)
    })(i)
}

fn options(i: &str) -> IResult<&str, Line> {
    let (i, options) = separated_nonempty_list(
        line_ending,
        pair(
            terminated(
                preceded(tag("[["), separated_pair(string, tag("|"), word)),
                tag("]]"),
            ),
            opt(preceded(space1, l10n_tag)),
        ),
    )(i)?;
    Ok((
        i,
        Line::Options(
            options
                .into_iter()
                .map(|((text, title), tag)| (text, title, tag))
                .collect(),
        ),
    ))
}

// TODO: Add jump, a choice-less move to another node

fn line(i: &str) -> IResult<&str, Line> {
    alt((options, shortcuts, dialogue))(i)
}

fn body_end(i: &str) -> IResult<&str, &str> {
    terminated(tag("==="), opt(end_of_line))(i)
}

fn body(i: &str) -> IResult<&str, Vec<Line>> {
    terminated(
        separated_list(end_of_line, line),
        preceded(end_of_line, body_end),
    )(i)
}

fn node(i: &str) -> IResult<&str, Node> {
    let (i, props) = header(i)?;
    let (i, lines) = body(i)?;
    Ok((
        i,
        Node {
            meta: props,
            lines: lines,
        },
    ))
}

pub fn file(i: &str) -> IResult<&str, Vec<Node>> {
    all_consuming(many1(node))(i)
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;
    use std::fs;

    #[test]
    fn test_word() {
        assert_eq!(word(" blah"), Err(Error((" blah", ErrorKind::TakeWhile1))));
        assert_eq!(word("foo"), Ok(("", "foo")));
        assert_eq!(word("foo bar"), Ok((" bar", "foo")));
        assert_eq!(word("D3ADB33F "), Ok((" ", "D3ADB33F")));
        assert_eq!(word("some-arbitrary-text"), Ok(("", "some-arbitrary-text")));
        assert_eq!(word("DrawnHere_No"), Ok(("", "DrawnHere_No")));
    }

    #[test]
    fn test_number() {
        assert_eq!(number("1000"), Ok(("", 1000)));
        assert_eq!(number("911 foo"), Ok((" foo", 911)));
    }

    #[test]
    fn test_string() {
        assert_eq!(
            string("How delightful! #line:a8e70c\n"),
            Ok(("#line:a8e70c\n", "How delightful!"))
        );
        assert_eq!(string("How delightful!\n"), Ok(("\n", "How delightful!")));
    }

    #[test]
    fn test_colon_separator() {
        assert_eq!(colon_separator(":"), Ok(("", ":")));
        assert_eq!(colon_separator(": "), Ok(("", ":")));
        assert_eq!(colon_separator(" :"), Ok(("", ":")));
        assert_eq!(colon_separator(" : "), Ok(("", ":")));
    }

    #[test]
    fn test_property() {
        assert_eq!(property("foo:bar\n"), Ok(("", ("foo", Some("bar")))));
        assert_eq!(property("foo: bar\n"), Ok(("", ("foo", Some("bar")))));
        assert_eq!(property("colorID: 0\n"), Ok(("", ("colorID", Some("0")))));
        assert_eq!(
            property("tags: foo bar\n"),
            Ok(("", ("tags", Some("foo bar"))))
        );
        assert_eq!(
            property("position: 592,181\n"),
            Ok(("", ("position", Some("592,181"))))
        );
        assert_eq!(property("tags: \n"), Ok(("", ("tags", None))));
    }

    #[test]
    fn test_list() {
        assert_eq!(
            list("tags: \n"),
            Err(Error(("tags: \n", ErrorKind::Verify)))
        );
        assert_eq!(
            list("tags: foo\n"),
            Err(Error(("tags: foo\n", ErrorKind::Verify)))
        );
        assert_eq!(
            list("tags: foo bar\n"),
            Ok(("", ("tags", Some(Value::List(vec!["foo", "bar"])))))
        );
        assert_eq!(
            list("tags: foo bar \n"),
            Ok(("", ("tags", Some(Value::List(vec!["foo", "bar"])))))
        );
        assert_eq!(
            list("tags: foo bar  baz\n"),
            Ok(("", ("tags", Some(Value::List(vec!["foo", "bar", "baz"])))))
        );
    }

    #[test]
    fn test_coordinate() {
        assert_eq!(
            coordinate("position:\n"),
            Err(Error(("position:\n", ErrorKind::Verify)))
        );
        assert_eq!(
            coordinate("position: 592,181\n"),
            Ok(("", ("position", Some(Value::Coordinate((592, 181))))))
        );
    }

    #[test]
    fn test_prop_word() {
        assert_eq!(
            prop_word("tags: foo bar\n"),
            Err(Error(("tags: foo bar\n", ErrorKind::Verify)))
        );
        assert_eq!(
            prop_word("title: Start\n"),
            Ok(("", ("title", Some(Value::Word("Start")))))
        );
    }

    #[test]
    fn test_header_end() {
        assert_eq!(header_end("---\n"), Ok(("", "---")));
    }

    #[test]
    fn test_properties() {
        assert_eq!(properties("tags: \n"), Ok(("", ("tags", None))));
        assert_eq!(
            properties("title: Start\n"),
            Ok(("", ("title", Some(Value::Word("Start")))))
        );
        assert_eq!(
            properties("position: 592,181\n"),
            Ok(("", ("position", Some(Value::Coordinate((592, 181))))))
        );
        assert_eq!(
            properties("tags: foo bar\n"),
            Ok(("", ("tags", Some(Value::List(vec!["foo", "bar"])))))
        );
    }

    #[test]
    fn test_header() {
        assert_eq!(
            header(
                "\
                title: Start\n\
                tags:\n\
                colorID: 0\n\
                position: 592,181\n\
                ---\n"
            ),
            Ok((
                "",
                vec![
                    ("title", Some(Value::Word("Start"))),
                    ("tags", None),
                    ("colorID", Some(Value::Word("0"))),
                    ("position", Some(Value::Coordinate((592, 181))))
                ]
            ))
        );
    }

    #[test]
    fn test_l10n_tag() {
        assert_eq!(l10n_tag("#line:a8e70c"), Ok(("", "a8e70c")));
        assert_eq!(
            l10n_tag("#line:"),
            Err(Error(("", ErrorKind::AlphaNumeric)))
        );
    }

    #[test]
    fn test_dialogue() {
        assert_eq!(
            dialogue("A: Hey, I'm a character in a script!\n"),
            Ok((
                "\n",
                Line::Dialogue((Some("A"), "Hey, I'm a character in a script!", None))
            ))
        );
        assert_eq!(
            dialogue("And I am too! You are talking to me!\n"),
            Ok((
                "\n",
                Line::Dialogue((None, "And I am too! You are talking to me!", None))
            ))
        );
        assert_eq!(
            dialogue("What would you prefer to do next? #line:a8e70c\n"),
            Ok((
                "\n",
                Line::Dialogue((None, "What would you prefer to do next?", Some("a8e70c")))
            ))
        );
        assert_eq!(
            dialogue("A: Oh, goodbye! #line:a8e70c\n"),
            Ok((
                "\n",
                Line::Dialogue((Some("A"), "Oh, goodbye!", Some("a8e70c")))
            ))
        );
        assert_eq!(dialogue("===\n"), Err(Error(("===\n", ErrorKind::Verify))));
    }

    #[test]
    fn test_shortcut() {
        assert_eq!(
            shortcuts("-> What's going on\n"),
            Ok((
                "\n",
                Line::Shortcuts(vec![("What's going on", vec![], None, None)])
            ))
        );
        assert_eq!(
            shortcuts("-> Um ok #line:a8e70c\n"),
            Ok((
                "\n",
                Line::Shortcuts(vec![("Um ok", vec![], Some("a8e70c"), None)])
            ))
        );
        assert_eq!(
            shortcuts("-> What's going on #line:a8e70c\n\tA: Why this is a demo of the script system!\n\tB: And you're in it!\n-> Um ok\n"),
            Ok(("\n", Line::Shortcuts(vec![("What's going on",
                vec![
                    Line::Dialogue((Some("A"), "Why this is a demo of the script system!", None)),
                    Line::Dialogue((Some("B"), "And you're in it!", None))
                ], Some("a8e70c"), None), ("Um ok", vec![], None, None)])))
        );
    }

    #[test]
    fn test_option() {
        assert_eq!(
            options("[[Learn more|LearnMore]]\n"),
            Ok(("\n", Line::Options(vec![("Learn more", "LearnMore", None)])))
        );
        assert_eq!(
            options("[[Learn more|LearnMore]] #line:a8e70c\n"),
            Ok((
                "\n",
                Line::Options(vec![("Learn more", "LearnMore", Some("a8e70c"))])
            ))
        );
        assert_eq!(
            options("[[I don't think so. I wish.|DrawnHere_No]]\n"),
            Ok((
                "\n",
                Line::Options(vec![("I don't think so. I wish.", "DrawnHere_No", None)])
            ))
        );
        assert_eq!(
            options("[[Leave|Leave]]\n[[Learn more|LearnMore]]\n"),
            Ok((
                "\n",
                Line::Options(vec![
                    ("Leave", "Leave", None),
                    ("Learn more", "LearnMore", None)
                ])
            ))
        )
    }

    #[test]
    fn test_body_end() {
        assert_eq!(body_end("==="), Ok(("", "===")));
        assert_eq!(body_end("===\n"), Ok(("", "===")));
        assert_eq!(body_end("=== \n"), Ok(("", "===")));
    }

    #[test]
    fn test_line() {
        assert_eq!(
            line("A: Hey, I'm a character in a script!\n"),
            Ok((
                "\n",
                Line::Dialogue((Some("A"), "Hey, I'm a character in a script!", None))
            ))
        );
        assert_eq!(
            line("-> What's going on\n"),
            Ok((
                "\n",
                Line::Shortcuts(vec![("What's going on", vec![], None, None)])
            ))
        );
        assert_eq!(
            line("[[Learn more|LearnMore]]\n"),
            Ok(("\n", Line::Options(vec![("Learn more", "LearnMore", None)])))
        );
    }

    #[test]
    fn test_body() {
        assert_eq!(
            body(
                "\
                A: Hey, I'm a character in a script!\n\
                B: And I am too! You are talking to me!\n\
                -> What's going on\n\
                \tA: Why this is a demo of the script system!\n\
                \tB: And you're in it!\n\
                -> Um ok\n\
                A: How delightful!\n\
                B: What would you prefer to do next?\n\
                [[Leave|Leave]]\n\
                [[Learn more|LearnMore]]\n\
                ===\n"
            ),
            Ok((
                "",
                vec![
                    Line::Dialogue((Some("A"), "Hey, I'm a character in a script!", None)),
                    Line::Dialogue((Some("B"), "And I am too! You are talking to me!", None)),
                    Line::Shortcuts(vec![
                        (
                            "What's going on",
                            vec![
                                Line::Dialogue((
                                    Some("A"),
                                    "Why this is a demo of the script system!",
                                    None
                                )),
                                Line::Dialogue((Some("B"), "And you're in it!", None))
                            ],
                            None,
                            None
                        ),
                        ("Um ok", vec![], None, None)
                    ]),
                    Line::Dialogue((Some("A"), "How delightful!", None)),
                    Line::Dialogue((Some("B"), "What would you prefer to do next?", None)),
                    Line::Options(vec![
                        ("Leave", "Leave", None),
                        ("Learn more", "LearnMore", None)
                    ]),
                ]
            ))
        )
    }

    #[test]
    fn test_node() {
        assert_eq!(
            node(
                "\
                title: TestNode\n\
                colorID: 3\n\
                ---\n\
                A: Hey, I'm a character in a script!\n\
                B: And I am too! You are talking to me!\n\
                -> What's going on\n\
                \tA: Why this is a demo of the script system!\n\
                \tB: And you're in it!\n\
                -> Um ok\n\
                A: How delightful!\n\
                B: What would you prefer to do next?\n\
                [[Leave|Leave]]\n\
                [[Learn more|LearnMore]]\n\
                ===\n"
            ),
            Ok((
                "",
                Node {
                    meta: vec![
                        ("title", Some(Value::Word("TestNode"))),
                        ("colorID", Some(Value::Word("3")))
                    ],
                    lines: vec![
                        Line::Dialogue((Some("A"), "Hey, I'm a character in a script!", None)),
                        Line::Dialogue((Some("B"), "And I am too! You are talking to me!", None)),
                        Line::Shortcuts(vec![
                            (
                                "What's going on",
                                vec![
                                    Line::Dialogue((
                                        Some("A"),
                                        "Why this is a demo of the script system!",
                                        None
                                    )),
                                    Line::Dialogue((Some("B"), "And you're in it!", None))
                                ],
                                None,
                                None
                            ),
                            ("Um ok", vec![], None, None)
                        ]),
                        Line::Dialogue((Some("A"), "How delightful!", None)),
                        Line::Dialogue((Some("B"), "What would you prefer to do next?", None)),
                        Line::Options(vec![
                            ("Leave", "Leave", None),
                            ("Learn more", "LearnMore", None)
                        ])
                    ]
                }
            ))
        );
    }

    #[test]
    fn test_file() {
        let raw_file =
            fs::read_to_string("test-yarns/Example.yarn.txt").expect("can't read/find file");
        assert_eq!(
            file(&raw_file),
            Ok((
                "",
                vec![
                    Node {
                        meta: vec![
                            ("title", Some(Value::Word("Start"))),
                            ("tags", None),
                            ("colorID", Some(Value::Word("0"))),
                            ("position", Some(Value::Coordinate((592, 181))))
                        ],
                        lines: vec![
                            Line::Dialogue((Some("A"), "Hey, I'm a character in a script!", None)),
                            Line::Dialogue((
                                Some("B"),
                                "And I am too! You are talking to me!",
                                None
                            )),
                            Line::Shortcuts(vec![
                                (
                                    "What's going on",
                                    vec![
                                        Line::Dialogue((
                                            Some("A"),
                                            "Why this is a demo of the script system!",
                                            None
                                        )),
                                        Line::Dialogue((Some("B"), "And you're in it!", None))
                                    ],
                                    None,
                                    None
                                ),
                                ("Um ok", vec![], None, None)
                            ]),
                            Line::Dialogue((Some("A"), "How delightful!", None)),
                            Line::Dialogue((Some("B"), "What would you prefer to do next?", None)),
                            Line::Options(vec![
                                ("Leave", "Leave", None),
                                ("Learn more", "LearnMore", None)
                            ])
                        ]
                    },
                    Node {
                        meta: vec![
                            ("title", Some(Value::Word("Leave"))),
                            ("tags", None),
                            ("colorID", Some(Value::Word("0"))),
                            ("position", Some(Value::Coordinate((387, 487))))
                        ],
                        lines: vec![
                            Line::Dialogue((Some("A"), "Oh, goodbye!", None)),
                            Line::Dialogue((Some("B"), "You'll be back soon!", None))
                        ]
                    },
                    Node {
                        meta: vec![
                            ("title", Some(Value::Word("LearnMore"))),
                            ("tags", Some(Value::Word("rawText"))),
                            ("colorID", Some(Value::Word("0"))),
                            ("position", Some(Value::Coordinate((763, 472))))
                        ],
                        lines: vec![Line::Dialogue((Some("A"), "HAHAHA", None))]
                    }
                ]
            ))
        );
    }
}
