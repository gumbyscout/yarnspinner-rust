use super::parser;
use super::{Num, Parsed};
use std::borrow::ToOwned;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum PropValue {
    Coordinate((Num, Num)),
    Word(String),
    Number(Num),
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub enum Command {
    Conditional,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Line {
    Dialogue((Option<String>, String, Option<String>)),
    Shortcuts(Vec<(String, Vec<Line>, Option<String>, Option<Command>)>),
    Options(Vec<(String, String, Option<String>)>),
    Jump(String),
    Command(Command),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Node {
    pub title: String,
    pub tags: Vec<String>,
    pub props: HashMap<String, PropValue>,
    pub lines: Vec<Line>,
}

#[derive(Serialize, Deserialize)]
pub struct Compiler {
    // TODO: is there a cleaner way to do this that isn't a Line reference (avoiding lifetime specifiers).
    current_node: String,
    current_line: usize,
    last_node: Option<String>,
    showing_options: bool,
    pub nodes: HashMap<String, Node>,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct Dialogue {
    pub speaker: Option<String>,
    pub line: String,
    l10n_tag: Option<String>, // TODO: Do we need this passed to a consumer, or do we handle this?
}

#[derive(Debug, PartialEq, Serialize)]
pub struct OptionItem {
    pub line: String,
    l10n_tag: Option<String>, // TODO: Do we need this passed to a consumer, or do we handle this?
}

#[derive(Debug, PartialEq, Serialize)]
pub enum CompiledLine {
    Dialogue(Dialogue),
    Command(Command),
    Options(Vec<OptionItem>),
}

impl CompiledLine {
    // This constructor is gross with the borrowing, but it's just to dry up the next in the compiler.
    fn dialogue(speaker: &Option<String>, line: &String, l10n_tag: &Option<String>) -> Self {
        CompiledLine::Dialogue(Dialogue {
            speaker: speaker.to_owned(),
            line: line.to_owned(),
            l10n_tag: l10n_tag.to_owned(),
        })
    }
}

impl Iterator for Compiler {
    type Item = CompiledLine;

    fn next(&mut self) -> Option<CompiledLine> {
        if self.showing_options {
            return None;
        }
        let current = self.nodes.get(&self.current_node)?;
        let line = current.lines.get(self.current_line)?;
        let next_line = self.current_line + 1;
        match line {
            Line::Dialogue((speaker, line, l10n_tag)) => {
                self.current_line = next_line;
                Some(CompiledLine::dialogue(speaker, line, l10n_tag))
            }
            Line::Jump(title) => {
                self.jump(&title.to_owned());
                self.next()
            }
            Line::Command(c) => {
                self.current_line = next_line;
                Some(CompiledLine::Command(c.clone()))
            }
            Line::Options(o) => {
                self.showing_options = true;
                Some(CompiledLine::Options(
                    o.iter()
                        .map(|(d, _, l)| OptionItem {
                            line: d.to_owned(),
                            l10n_tag: l.to_owned(),
                        })
                        .collect(),
                ))
            }
            Line::Shortcuts(s) => {
                self.showing_options = true;
                Some(CompiledLine::Options(
                    s.iter()
                        // TODO: Don't return options that aren't 'true'
                        .map(|(d, _, l, _)| OptionItem {
                            line: d.to_owned(),
                            l10n_tag: l.to_owned(),
                        })
                        .collect(),
                ))
            }
        }
    }
}

impl Compiler {
    pub fn new(starting_node: &str) -> Self {
        Self {
            current_node: starting_node.into(),
            last_node: None,
            showing_options: false,
            current_line: 0,
            nodes: HashMap::new(),
        }
    }

    pub fn compile(&mut self, parsed: Parsed) -> Result<(), String> {
        let nodes = parsed?;
        for node in nodes {
            let node = parsed_node_to_node(node)?;
            self.nodes.insert(node.title.to_owned(), node);
        }
        Ok(())
    }

    fn jump(&mut self, title: &str) {
        self.last_node = Some(self.current_node.to_owned());
        self.current_node = self
            .nodes
            .get(title)
            .map(|n| n.title.to_owned())
            .unwrap_or(String::from(""));
        self.current_line = 0;
    }
    // TODO: Clean this up when generators exist?
    pub fn answer(&mut self, option: &str) -> Result<(), String> {
        let current = self
            .nodes
            .get(&self.current_node)
            .ok_or(String::from("couldn't find current node"))?;
        let line = current
            .lines
            .get(self.current_line)
            .ok_or(String::from("couldn't get current line"))?;
        match line {
            Line::Options(o) => {
                let (_, t, _) = o
                    .iter()
                    .find(|(d, _, _)| option == *d)
                    .ok_or(String::from("answer not found"))?;
                self.jump(&t.to_owned());
                self.showing_options = false;
                Ok(())
            }
            Line::Shortcuts(s) => {
                // TODO: run inner lines
                let shortcut = s
                    .iter()
                    .find(|(d, _, _, _)| option == *d)
                    .ok_or(String::from("answer not found"))?;
                self.current_line = self.current_line + 1;
                self.showing_options = false;
                Ok(())
            }
            _ => Err(String::from("answer not found")),
        }
    }
}

fn title_from_meta(i: &Vec<parser::Property>) -> Result<String, String> {
    let title = i
        .iter()
        .find(|(k, _)| k == &"title")
        .and_then(|(_, v)| v.clone())
        .and_then(|v| match v {
            parser::Value::Word(t) => Some(t),
            _ => None,
        })
        .map(String::from)
        .ok_or(format!("node missing title: {:?}", i))?;
    Ok(title)
}

fn tags_from_meta(i: &Vec<parser::Property>) -> Result<Vec<String>, String> {
    let tags = i
        .iter()
        .find(|(k, _)| k == &"tags")
        .and_then(|(_, v)| v.clone())
        .and_then(|v| match v {
            parser::Value::List(t) => Some(t),
            _ => None,
        })
        .map(|l| l.into_iter().map(String::from).collect())
        .ok_or(format!("node missing tags: {:?}", i))?;
    Ok(tags)
}

fn props_from_meta(i: &Vec<parser::Property>) -> HashMap<String, PropValue> {
    let mut props = HashMap::new();
    for (k, v) in i {
        if k == &"title" || k == &"tags" || v.is_none() {
            continue;
        }
        match v.clone().unwrap() {
            parser::Value::Word(w) => {
                props.insert(String::from(*k), PropValue::Word(String::from(w)));
            }
            parser::Value::Number(n) => {
                props.insert(String::from(*k), PropValue::Number(n));
            }
            parser::Value::Coordinate((x, y)) => {
                props.insert(String::from(*k), PropValue::Coordinate((x, y)));
            }
            _ => (),
        }
    }
    props
}

fn map_command(i: parser::Command) -> Command {
    match i {
        parser::Command::Conditional => Command::Conditional,
    }
}

fn map_line(i: parser::Line) -> Line {
    match i {
        parser::Line::Command(c) => Line::Command(map_command(c)),
        parser::Line::Dialogue((s, d, l)) => {
            Line::Dialogue((s.map(String::from), String::from(d), l.map(String::from)))
        }
        parser::Line::Jump(j) => Line::Jump(String::from(j)),
        parser::Line::Options(o) => Line::Options(
            o.into_iter()
                .map(|(d, t, l)| (String::from(d), String::from(t), l.map(String::from)))
                .collect(),
        ),
        parser::Line::Shortcuts(s) => Line::Shortcuts(
            // TODO: Handle the command
            s.into_iter()
                .map(|(d, n, l, _)| {
                    (
                        String::from(d),
                        n.into_iter().map(map_line).collect(),
                        l.map(String::from),
                        None,
                    )
                })
                .collect(),
        ),
    }
}

fn parsed_node_to_node(i: parser::Node) -> Result<Node, String> {
    let title = title_from_meta(&i.meta)?;
    let tags = tags_from_meta(&i.meta).unwrap_or(vec![]);
    let props = props_from_meta(&i.meta);
    Ok(Node {
        title,
        tags,
        props,
        lines: i.lines.into_iter().map(map_line).collect(),
    })
}

#[cfg(test)]
mod tests {
    use super::super::parse;
    use super::*;
    use pretty_assertions::assert_eq;
    use std::fs;

    fn make_compiler() -> Compiler {
        let raw_file =
            fs::read_to_string("test-yarns/Example.yarn.txt").expect("can't read/find file");
        let parsed = parse(&raw_file);
        let mut compiler = Compiler::new("Start");
        compiler.compile(parsed).expect("failed to compile");
        compiler
    }

    #[test]
    fn test_compiler_next() {
        let mut compiler = make_compiler();
        assert_eq!(
            compiler.next(),
            Some(CompiledLine::dialogue(
                &Some("A".into()),
                &"Hey, I'm a character in a script!".into(),
                &None.into(),
            ))
        );
        assert_eq!(
            compiler.next(),
            Some(CompiledLine::dialogue(
                &Some("B".into()),
                &"And I am too! You are talking to me!".into(),
                &None.into(),
            ))
        );
        assert_eq!(
            compiler.next(),
            Some(CompiledLine::Options(vec![
                OptionItem {
                    line: "What's going on".into(),
                    l10n_tag: None,
                },
                OptionItem {
                    line: "Um ok".into(),
                    l10n_tag: None,
                }
            ]))
        );
        assert_eq!(compiler.next(), None);
        assert_eq!(compiler.answer("Um ok"), Ok(()));
        assert_eq!(
            compiler.next(),
            Some(CompiledLine::dialogue(
                &Some("A".into()),
                &"How delightful!".into(),
                &None.into()
            ))
        );
        assert_eq!(
            compiler.next(),
            Some(CompiledLine::dialogue(
                &Some("B".into()),
                &"What would you prefer to do next?".into(),
                &None.into()
            ))
        );
        assert_eq!(
            compiler.next(),
            Some(CompiledLine::Options(vec![
                OptionItem {
                    line: "Leave".into(),
                    l10n_tag: None,
                },
                OptionItem {
                    line: "Learn more".into(),
                    l10n_tag: None,
                }
            ]))
        );
        assert_eq!(compiler.next(), None);
        assert_eq!(compiler.answer("Leave"), Ok(()));
        assert_eq!(
            compiler.next(),
            Some(CompiledLine::dialogue(
                &Some("A".into()),
                &"Oh, goodbye!".into(),
                &None.into()
            ))
        );
        assert_eq!(
            compiler.next(),
            Some(CompiledLine::dialogue(
                &Some("B".into()),
                &"You'll be back soon!".into(),
                &None.into()
            ))
        );
        assert_eq!(compiler.next(), None);
    }
}
