#[macro_use]
extern crate serde_derive;

extern crate nom;
mod compiler;
mod parser;

pub use compiler::{CompiledLine, Compiler, Dialogue, Line, Node, OptionItem, PropValue};

type Num = i32;

pub type Parsed<'a> = Result<Vec<parser::Node<'a>>, String>;

pub fn parse(file: &str) -> Parsed {
    let (_, nodes) = parser::file(file).map_err(|e| e.to_string())?;
    Ok(nodes)
}
