extern crate gdnative;

use gdnative::init::{property, ExportInfo, PropertyUsage, Signal, SignalArgument};
use gdnative::*;
use serde_json;
use std::collections::HashMap;
use std::convert::From;
use std::sync::{Arc, Mutex};
use yarn_spinner::{CompiledLine, Compiler, PropValue};

#[derive(ToVariant, FromVariant)]
pub struct YarnNode {
    pub title: GodotString,
    pub tags: StringArray,
    pub props: Dictionary,
    pub lines: GodotString,
}

impl From<&yarn_spinner::Node> for YarnNode {
    fn from(node: &yarn_spinner::Node) -> Self {
        let lines = serde_json::to_string(&node.lines)
            .map(GodotString::from)
            .unwrap_or_default();

        let tags = node.tags.to_variant().to_string_array();
        let mut props = Dictionary::new();
        for (k, v) in &node.props {
            let v = match v {
                PropValue::Word(w) => w.to_variant(),
                PropValue::Number(n) => n.to_variant(),
                PropValue::Coordinate((x, y)) => Vector2::new(*x as f32, *y as f32).to_variant(),
            };
            props.set(&k.to_variant(), &v);
        }
        Self {
            title: node.title.to_owned().into(),
            tags: tags,
            props: props,
            lines: lines,
        }
    }
}

impl From<YarnNode> for yarn_spinner::Node {
    fn from(node: YarnNode) -> Self {
        let lines: Vec<yarn_spinner::Line> = serde_json::from_str(&node.lines.to_string()).unwrap();
        // TODO: Supposedly you can do Variant => Vec<T> but I couldn't
        // figure it out.
        let tags: Vec<String> = node
            .tags
            .to_variant()
            .to_array()
            .iter()
            .map(|t| t.to_string())
            .collect();
        let mut props = HashMap::<String, PropValue>::new();
        for key in node.props.keys().iter() {
            // TODO: Is there a way to match off of a variant's type?
            let val = node.props.get(key);
            let res = val.try_to_string().map(|v| {
                props.insert(key.to_string(), PropValue::Word(v));
            });
            // TODO: Figure out how chaining off of failures works.
            if res.is_some() {
                continue;
            }
            let res = val.try_to_vector2().map(|v| {
                props.insert(
                    key.to_string(),
                    PropValue::Coordinate((v.x as i32, v.y as i32)),
                );
            });
            if res.is_some() {
                continue;
            }
            let res = val.try_to_f64().map(|v| {
                props.insert(key.to_string(), PropValue::Number(v as i32));
            });
            if res.is_none() {
                godot_error!("failed to coercice prop: {:?}", key.to_string());
            }
        }
        Self {
            title: node.title.to_string(),
            tags: tags,
            props: props,
            lines: lines,
        }
    }
}

#[derive(NativeClass)]
#[inherit(Resource)]
#[register_with(Self::register)]
pub struct Yarn {
    pub nodes: Dictionary,
    raw: String,
}

#[methods]
impl Yarn {
    fn _init(_owner: Resource) -> Self {
        Self {
            nodes: Dictionary::new(),
            raw: String::new(),
        }
    }

    fn register(builder: &init::ClassBuilder<Self>) {
        builder
            .add_property::<Dictionary>("nodes")
            .with_getter(|this: &Yarn, _| this.nodes.to_variant().to_dictionary())
            .with_setter(|this: &mut Yarn, _, n| this.nodes = n)
            .done();
        builder
            .add_property::<String>("raw")
            .with_getter(|this: &Yarn, _| this.raw.to_owned())
            .with_setter(|this: &mut Yarn, _, s| this.raw = s)
            .with_hint(property::hint::StringHint::Multiline)
            .done();
    }

    pub fn add_node(&mut self, node: YarnNode) {
        self.nodes.set(&node.title.to_variant(), &node.to_variant());
    }
}

#[derive(NativeClass)]
#[inherit(EditorImportPlugin)]
pub struct YarnImporter;

#[methods]
impl YarnImporter {
    const EXTENSION: &'static str = "tres";
    fn _init(_owner: EditorImportPlugin) -> Self {
        Self
    }

    #[export]
    pub fn get_importer_name(&mut self, _owner: EditorImportPlugin) -> GodotString {
        GodotString::from("yarn.importer")
    }

    #[export]
    pub fn get_visible_name(&mut self, _owner: EditorImportPlugin) -> GodotString {
        GodotString::from("Yarn Importer")
    }

    #[export]
    pub fn get_recognized_extensions(&mut self, _owner: EditorImportPlugin) -> VariantArray {
        let mut a = VariantArray::new();
        a.push(&GodotString::from("yarn").to_variant());
        a.push(&GodotString::from("txt").to_variant());
        a
    }

    #[export]
    pub fn get_save_extension(&mut self, _owner: EditorImportPlugin) -> GodotString {
        GodotString::from(Self::EXTENSION)
    }

    #[export]
    pub fn get_resource_type(&mut self, _owner: EditorImportPlugin) -> GodotString {
        // Returning "Yarn" doesn't work, and makes it to where this
        // resource isn't usable as a resource.
        GodotString::from("Resource")
    }

    #[export]
    pub fn get_preset_count(&mut self, _owner: EditorImportPlugin) -> i64 {
        0
    }

    #[export]
    pub fn get_import_options(&mut self, _owner: EditorImportPlugin, _preset: i64) -> VariantArray {
        VariantArray::new()
    }

    pub fn import_res(
        source_file: GodotString,
        save_path: GodotString,
        _options: Dictionary,
        _platform_variants: VariantArray,
        _gen_files: VariantArray,
    ) -> Result<(), GodotError> {
        godot_print!("import: {:?} {:?}", source_file, save_path);
        let mut file = File::new();
        file.open(source_file, File::READ)?;

        let contents = file.get_as_text().to_string();
        let res = yarn_spinner::parse(&contents);
        let mut compiler = Compiler::new("");
        compiler.compile(res).map_err(|e| {
            godot_error!("yarn compiler failed: {:?}", e);
            GodotError::Failed
        })?;
        let nodes: Vec<YarnNode> = compiler
            .nodes
            .iter()
            .map(|(_, n)| YarnNode::from(n))
            .collect();
        let filename = format!("{}.{}", save_path.to_string(), Self::EXTENSION);
        let yarn = Instance::<Yarn>::new();
        yarn.map_mut(|y, _| {
            y.raw = contents.to_owned();
            for node in nodes {
                y.add_node(node);
            }
        })
        .map_err(|e| {
            godot_error!("failed to mutate Yarn: {:?}", e);
            GodotError::Failed
        })?;
        ResourceSaver::godot_singleton().save(
            GodotString::from(&filename),
            Some(yarn.into_base()),
            0,
        )?;
        Ok(())
    }

    #[export]
    pub fn import(
        &mut self,
        _owner: EditorImportPlugin,
        source_file: GodotString,
        save_path: GodotString,
        options: Dictionary,
        platform_variants: VariantArray,
        gen_files: VariantArray,
    ) -> i64 {
        let res = Self::import_res(
            source_file,
            save_path,
            options,
            platform_variants,
            gen_files,
        );
        if res.is_err() {
            let e = res.unwrap_err();
            godot_error!("import error: {:?}", e);
            return GodotError::Failed as i64;
        }
        0
    }
}

#[derive(NativeClass)]
#[inherit(Node)]
#[register_with(Self::register)]
pub struct YarnRunner {
    starting_node: String,
    compiler: Arc<Mutex<Compiler>>,
}

#[methods]
impl YarnRunner {
    fn _init(_owner: Node) -> Self {
        Self {
            starting_node: "".into(),
            compiler: Arc::new(Mutex::new(Compiler::new(""))),
        }
    }

    fn register(builder: &init::ClassBuilder<Self>) {
        builder
            .add_property::<String>("starting_node")
            .with_getter(|this: &YarnRunner, _| this.starting_node.to_owned())
            .with_setter(|this: &mut YarnRunner, _, v| this.starting_node = v)
            .done();
        builder.add_signal(Signal {
            name: "line",
            args: &[
                SignalArgument {
                    name: "speaker",
                    default: "".into(),
                    export_info: ExportInfo::new(VariantType::GodotString),
                    usage: PropertyUsage::DEFAULT,
                },
                SignalArgument {
                    name: "line",
                    default: "".into(),
                    export_info: ExportInfo::new(VariantType::GodotString),
                    usage: PropertyUsage::DEFAULT,
                },
            ],
        });

        builder.add_signal(Signal {
            name: "options",
            args: &[SignalArgument {
                name: "options",
                default: Vec::<String>::new().to_variant(),
                export_info: ExportInfo::new(VariantType::StringArray),
                usage: PropertyUsage::DEFAULT,
            }],
        });

        builder.add_signal(Signal {
            name: "answered",
            args: &[],
        });

        builder.add_signal(Signal {
            name: "done",
            args: &[],
        });
    }

    #[export]
    unsafe fn _ready(&mut self, owner: Node) {
        let mut compiler = Compiler::new(&self.starting_node);
        let nodes = owner.get_children();
        for node in nodes.iter() {
            let node: Node = node.try_to_object().unwrap();
            let res = Instance::<Yarn>::from_variant(&node.get("yarn".into()));
            if res.is_err() {
                godot_print!("missing 'yarn' class property");
                continue;
            }
            res.unwrap()
                .map(|y, _| {
                    for key in y.nodes.keys().iter() {
                        match YarnNode::from_variant(&y.nodes.get(key)) {
                            Ok(n) => {
                                let node: yarn_spinner::Node = n.into();
                                compiler.nodes.insert(node.title.to_owned(), node);
                            }
                            Err(e) => godot_error!("failed to get YarnNode variant: {:?}", e),
                        };
                    }
                })
                .unwrap();
        }
        self.compiler = Arc::new(Mutex::new(compiler));
    }

    #[export]
    unsafe fn next(&self, mut owner: Node) {
        let compiler = Arc::clone(&self.compiler);
        match compiler.try_lock().as_mut() {
            Ok(c) => match c.next() {
                None => {
                    owner.emit_signal(GodotString::from("done"), &[]);
                }
                Some(x) => match x {
                    CompiledLine::Dialogue(d) => {
                        owner.emit_signal(
                            GodotString::from("line"),
                            &[
                                Variant::from_godot_string(&GodotString::from(
                                    d.speaker.unwrap_or("".into()),
                                )),
                                Variant::from_godot_string(&GodotString::from(d.line)),
                            ],
                        );
                    }
                    CompiledLine::Options(o) => {
                        owner.emit_signal(
                            GodotString::from("options"),
                            &[o.iter()
                                .map(|o| o.line.to_owned())
                                .collect::<Vec<String>>()
                                .to_variant()],
                        );
                    }
                    _ => (),
                },
            },
            Err(e) => godot_error!("lock: {:?}", e),
        };
    }

    #[export]
    unsafe fn answer(&self, mut owner: Node, option: GodotString) {
        let compiler = Arc::clone(&self.compiler);
        match compiler.try_lock().as_mut() {
            Ok(c) => match c.answer(&option.to_string()) {
                Ok(()) => {}
                Err(e) => godot_error!("failed to answer: {:?}", e),
            },
            Err(e) => godot_error!("lock: {:?}", e),
        };
        owner.emit_signal(GodotString::from("answered"), &[]);
    }
}

// Function that registers all exposed classes to Godot
fn init(handle: gdnative::init::InitHandle) {
    handle.add_tool_class::<Yarn>();
    handle.add_tool_class::<YarnImporter>();
    handle.add_class::<YarnRunner>();
}

// macros that create the entry-points of the dynamic library.
godot_gdnative_init!();
godot_nativescript_init!(init);
godot_gdnative_terminate!();
