extends Control

onready var Speaker: Label = $DialogueBox/VBoxContainer/Speaker
onready var Dialogue: RichTextLabel = $DialogueBox/VBoxContainer/Dialogue
onready var Options: Node = $DialogueBox/VBoxContainer/Options
onready var Runner: YarnRunner = $Runner

var _showing_options: bool

func _ready() -> void:
	# Get the first line of dialogue
	Runner.next()
	

func _on_Option_pressed(option: String) -> void:
	Runner.answer(option)
	

func _on_Runner_line(speaker: String, line: String) -> void:
	Speaker.text = speaker
	Dialogue.text = line


func _on_Runner_options(options: Array) -> void:
	_showing_options = true
	for option in options:
		if option is String:
			var btn := Button.new()
			btn.text = option
			# warning-ignore:return_value_discarded
			btn.connect("pressed", self, "_on_Option_pressed", [option])
			Options.add_child(btn)


func _on_Runner_answered() -> void:
	# Clear options
	_showing_options = false
	for option in Options.get_children():
		Options.remove_child(option)
	# Get next line of dialogue
	Runner.next()


func _on_Runner_done() -> void:
	if !_showing_options:
		get_tree().quit()


